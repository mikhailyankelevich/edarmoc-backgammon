package GAME;

/* Ilya Lyubomirov -- 16205331 */
/* Mikhail Yankelevich -- 16205326 */

/**
 * This class implements an ENUM that is used to differentiate players/checkers colours
 */
public enum Colour {
    BLACK, WHITE
}